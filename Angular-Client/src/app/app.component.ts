import { Component, OnInit, EventEmitter } from "@angular/core";
import { MessageService } from "primeng/api";

import { Router, ActivatedRoute, Params } from "@angular/router";

import { HubConnection, HubConnectionBuilder, LogLevel } from "@aspnet/signalr";
import { MichatService } from "./servicio/michat.service";
import { Mensaje } from "./modelo/mensaje";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
  providers: [MessageService]
})
export class AppComponent implements OnInit {
  private hubConnection: HubConnection;
  public mensaje: Mensaje;
  messageReceived = new EventEmitter<Mensaje>();
  constructor(
    private messageService: MessageService,
    private michatservice: MichatService
  ) {}

  ngOnInit(): void {
    this.michatservice.getMensaje().subscribe(x => {
      this.mensaje = <any>x;
      console.log(this.mensaje);
    });
    const hubConnection = new HubConnectionBuilder()
      .configureLogging(LogLevel.Information)
      .withUrl("http://localhost:5002/notify")
      .build();

    hubConnection
      .start()
      .then(function() {
        console.log("Connected!");
      })
      .catch(function(err) {
        return console.error(err.toString());
      });

    hubConnection.on("BroadcastMessage", (mensaje: Mensaje) => {
      this.michatservice.getMensaje().subscribe(x => {
        this.mensaje = <any>x;
        console.log(this.mensaje);
      });
    });
    /*   this.michatservice.getMensaje().subscribe(x => {
        this.mensaje = <any>x;
      }); */
  }
}
