import { Injectable } from "@angular/core";
import "rxjs/add/operator/map";
import { Observable } from "rxjs/Rx";
import { shareReplay } from "rxjs/operators";
import { Mensaje } from "../modelo/mensaje";
import {
  HttpClient,
  HttpHeaders,
  HttpResponse,
  HttpClientModule
} from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class MichatService {
  push(arg0: { type: string; mensaje: string }) {
    throw new Error("Method not implemented.");
  }
  add(arg0: { type: string; mensaje: string }) {
    throw new Error("Method not implemented.");
  }
  constructor(private http: HttpClient) {}
  guardarServicioProducto(mensaje: Mensaje) {
    /*  return this.http.post(this.url + 'dto', producto, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        })
      }).map(data => data);
  */
    let json = JSON.stringify(mensaje);
    let params = json;
    let headers = {
      headers: new HttpHeaders({ "Content-Type": "application/json" })
    };

    return this.http
      .post("http://localhost:5002/api/message", mensaje, headers)
      .map(res => res);
  }
  getMensaje() {
    return this.http.get("http://localhost:5002/api/message");
    /*   return this.http
        .get("http://localhost:5002/api/message")
        .map((res: Response) => res.json()); */
  }
}
