﻿using System.Threading.Tasks;
using SignalR_Hub.Modelos;

namespace SignalRHub
{
    public interface ITypedHubClient
    {
        Task BroadcastMessage(Elmensaje msg);
    }
}
