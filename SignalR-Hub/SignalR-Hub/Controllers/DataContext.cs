using System;
using Microsoft.EntityFrameworkCore;
using SignalR_Hub.Modelos;

namespace SignalR_Hub
{
    public class DataContext : DbContext
    {
        public DataContext() { }
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        public DbSet<Elmensaje> mensaje { get; set; }
    }
}