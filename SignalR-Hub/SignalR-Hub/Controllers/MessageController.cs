﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using SignalR_Hub.Modelos;
using SignalRHub;
using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;

namespace SignalR_Hub.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        private IHubContext<NotifyHub, ITypedHubClient> _hubContext;
        private DataContext context;

        public MessageController(IHubContext<NotifyHub, ITypedHubClient> hubContext, DataContext context)
        {
            _hubContext = hubContext;
            this.context = context;
        }

        [HttpPost]
        public async Task<ActionResult<Elmensaje>> Post([FromBody] Elmensaje msg)
        {
            string retMessage;

            try
            {
                context.Add(msg);
                await _hubContext.Clients.All.BroadcastMessage(msg);
                retMessage = "Success";
                context.SaveChanges();

                return Ok(retMessage);
            }
            catch (Exception e)
            {
                retMessage = e.ToString();
            }

            return Ok(retMessage);
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<Elmensaje>> Put([FromBody] Elmensaje msg, int id)
        {
            string retMessage;

            try
            {

                context.Entry(msg).State = EntityState.Modified;
                await _hubContext.Clients.All.BroadcastMessage(msg);
                retMessage = "Success";
                context.SaveChanges();

                return Ok(retMessage);
            }
            catch (Exception e)
            {
                retMessage = e.ToString();
            }

            return Ok(retMessage);
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Elmensaje>>> get()
        {
            string retMessage;

            try
            {
                var datos = await context.mensaje.ToListAsync();
                Elmensaje msg = new Elmensaje();
                /*  await _hubContext.Clients.All.BroadcastMessage(msg); */
                retMessage = "Success";
                string json = JsonConvert.SerializeObject(datos, Formatting.Indented);
                return Ok(datos);
            }
            catch (Exception e)
            {
                retMessage = e.ToString();
            }

            return Ok(retMessage);
        }
    }
}