using System;
using System.ComponentModel.DataAnnotations;

namespace SignalR_Hub.Modelos
{
    public class Elmensaje
    {

        [Key]
        public int Id { get; set; }
        public string message { get; set; }
        public string type { get; set; }
    }
}