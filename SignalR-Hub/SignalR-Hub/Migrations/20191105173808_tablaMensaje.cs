﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SignalRHub.Migrations
{
    public partial class tablaMensaje : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "mensaje",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    message = table.Column<string>(nullable: true),
                    type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mensaje", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "mensaje");
        }
    }
}
